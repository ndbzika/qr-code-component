# Frontend Mentor - QR code component solution

This is a solution to the [QR code component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/qr-code-component-iux_sIO_H). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

- [Overview](#overview)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
- [Author](#author)

## Overview

### Screenshot

![Apllication](./images/screenshot.png)

### Links

- Solution URL: [GitLab](https://gitlab.com/ndbzika/qr-code-component/-/tree/main)
- Live Site URL: [Netlify](https://cozy-puffpuff-9c5a09.netlify.app/)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- Mobile-first workflow

### What I learned

I've lerned how to make a Mobile-first workflow;

### Continued development

I want to remake this project later with React.

## Author

- Website - [Flávio Henrique Marques de Sousa](https://flaviohenriquedev.vercel.app/)
- Frontend Mentor - [@ndbzika](https://www.frontendmentor.io/profile/ndbzika)
